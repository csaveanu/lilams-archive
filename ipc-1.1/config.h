/* config.h.  Generated automatically by configure.  */
/* config.h.in.  Generated automatically from configure.in by autoheader.  */

/* Define as the return type of signal handlers (int or void).  */
#define RETSIGTYPE void

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Set when gnuplot should be run automaticaly */
# define NOT_RUN_GNUPLOT 0

/* Define if you have the getcwd function.  */
#define HAVE_GETCWD 1

/* Define if you have the signal function.  */
#define HAVE_SIGNAL 1

/* Define if you have the strdup function.  */
#define HAVE_STRDUP 1

/* Define if you have the strtol function.  */
#define HAVE_STRTOL 1

/* Define if you have the system function.  */
#define HAVE_SYSTEM 1

/* Define if you have the time function.  */
#define HAVE_TIME 1

/* Define if you have the <math.h> header file.  */
#define HAVE_MATH_H 1

/* Define if you have the <signal.h> header file.  */
#define HAVE_SIGNAL_H 1

/* Define if you have the <time.h> header file.  */
#define HAVE_TIME_H 1

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H 1

/* Define if you have the m library (-lm).  */
#define HAVE_LIBM 1

/* Name of package */
#define PACKAGE "ipc"

/* Version number of package */
#define VERSION "1.1"

