#!/usr/bin/env pythonw
"""
MSFit to Table v 0.2

New version, based on HTMLparser

A tool with GUI to collect data from 
a series of MSFit HTML identification files
in a unique table

Written using wxPython in Python, August 2004, Cosmin
Modified to be more general and flexible, Sept 2004
Included the HTML parser in the script, 2008-04-29

Alain Jacquier's laboratory, Institut Pasteur, Paris, France

INSTRUCTIONS:

Place the files with .html or .htm extension in a folder
From the File menu select Choose directory...
You will be able to see a list of the html files in your directory

From the File menu choose Collect to..., the global file will
be saved under the name you specify.

"""
import os, string, sys, re, wx
from string import lstrip, rstrip
#from MsFitHTMLparse02 import MSFit32ParserProt #now included
from HTMLParser import HTMLParser

pepseqRE = r"sequence=([A-Z]+)\*?\\"
peplinePATT = re.compile(pepseqRE)
pepseqmodRE = r"\)([A-Z]+)\*?\("
peplinePATTmod = re.compile(pepseqmodRE)

class MSFit32ParserProt(HTMLParser):
    """
    A parser for MSFit HTML results to obtain a
    dictionary of data corresponding to the page
    Only protein information is retained
    """
    def reset(self):
        HTMLParser.reset(self)
        self.parsedlist =[]
        #list of text contained between tags
        self.comments = []
        #list of comment texts
        self.aftercomment = 0
        #true if the text to be handled
        #follows a comment
        self.accesionRE = re.compile('> (.+)')
        #allows extraction of ORF name from the first table
        
        #two variables to allow start of scanning for  peptide sequences in file
        self.seeksequence = False
        self.seeksequencemod = False
        
        self.seqlist = []
        #contains the list of sequences of all peptides
        
    def handle_starttag(self, tag, attrs):
        #print tag, attrs
        pass
    
    def handle_comment(self, tag):
        """
        Overrides the handle_comment method
        Builds a list of all the found comments
        If a comment is found, the aftercomment
        variable is set to True
        """
        self.comments.append(tag)
        self.aftercomment = 1
        #if tag == 'delta_mass':
            #self.seeksequence = True
        
        
    def handle_data(self, text):
        """
        Overrides the handle_data method
        writes to a list the text following
        any comment - modifies self.parsedlist
        """
        #print "text", text
        if self.aftercomment:
            cleantext = text.strip('\n\r ./')
            self.parsedlist.append([self.comments[-1], cleantext])
            #append the last comment and the text that follows
            self.aftercomment = 0
            #don't get any other value until the next comment
            #appears
            #however obtain the sequence of a peptide if we are in an in_mass tags
        return 1
            
        
    def get_dictionary(self):
        """
        Reorganizes data and treats some special cases
        of data found after comment tags
        Should be called after closing the parser
        """
        reccounter = 0
        secreccounter = 0
        list_out = []
        list = self.parsedlist
        details = 0
        passedrank1 = 0
        
        for i in range(len(list)):
            if list[i][0] == 'rank':
                passedrank1 = 1
                if list[i+1][0] <> 'fraction_matched':
                    #we are not at the beginning of the details
                    #sometimes proteins have equal ranks
                    #we build the data dictionary for the identified proteins
                    reccounter+=1
                    list_out.append({'protdata':{'rank':list[i][1]}})
                    details = 0
                else:
                    #we are at the beginning of the details
                    details = 1
                    secreccounter+=1
            else:
                if details == 0 and passedrank1 == 1:
                    #not yet in details but passed the first rank tags
                    #take care first of the accesion numbers
                    if list[i][0] == 'accession_number':
                        matches = self.accesionRE.search(self.parsedlist[i][1])
                        if matches:
                            entries = matches.groups()
                            list[i][1]=entries[0]
                    list_out[reccounter-1]['protdata'][list[i][0]] = list[i][1]
                elif details == 1:
                    #we are in the details, we may build the peptide masses lists
                    if list[i][0] == 'protein_coverage':
                        list_out[secreccounter-1]['protdata'][list[i][0]] = list[i][1]
        return list_out

###################################
##### INTERFACE ###################
###################################
[ID_CHOOSEDIR,
ID_COLLECTTO,
ID_EXIT,
ID_ABOUT,
] = map(lambda init_ctrls: wx.NewId(), range(4))

def Read_Dir(dirname):
    """
    Recover information for all the files in a folder, recursively
    Collect data in a list of complete path names, returned
    
    """
    filelist = []
    for f in os.listdir(dirname):
        file = dirname + os.sep + f
        if os.path.isdir(file):
            Read_Dir(file)
        else:
            extension = os.path.splitext(f)[1]
            if extension in (".html", ".htm"):
                filelist.append(file)
    return filelist

def WriteListToFile(filename, outlist):
    """
    Writes a list of lists to a tab delimited text file
    """
    fout = open(filename, "w")
    for entry in outlist:
        for element in entry:
            fout.write(element+'\t')
        fout.write('\n')
    fout.close()
    return 1
    
class MainFrame(wx.Frame):
    def __init__(self, parent, id, title):    
        wx.Frame.__init__(self, parent, id, title,
        wx.DefaultPosition, (600, 600))
            
        self.temp_outfile = "temp_data"
        self.defaultdirname = "."
        self.filelist = []
        
        #default directory for saving, will be modified after first save
        #self.maxy = 0
        
        # Create the menu bar and items
        self.menubar = wx.MenuBar()
        
        filemenu = wx.Menu()
        filemenu.Append(ID_CHOOSEDIR, 'Ch&oose folder...\tCtrl+O', 'Choose folder with html MSFit files')
        filemenu.AppendSeparator()
        filemenu.Append(ID_COLLECTTO, 'Collec&t data to...\tCtrl+S', 'Parse files and save data to the specified file')
        filemenu.AppendSeparator()
        filemenu.Append(ID_EXIT, '&Quit\tCtrl+Q', 'Exit program')
        
        helpmenu = wx.Menu()
        helpmenu.Append(ID_ABOUT, 'About...\tCtrl+H', 'About this program')
        
        # Bind the events to methods 
        wx.EVT_MENU(self, ID_CHOOSEDIR, self.OnChooseFolder)
        wx.EVT_MENU(self, ID_EXIT, self.OnFileExit)
        wx.EVT_MENU(self, ID_COLLECTTO, self.OnCollectData)
        wx.EVT_MENU(self, ID_ABOUT, self.OnHelpAbout)
        
        # Attach menus to the menubar
        self.menubar.Append(filemenu, '&File')
        self.menubar.Append(helpmenu, '&Help')
            
        self.SetMenuBar(self.menubar)
        
        #Disable by default the  Save data as menu
        self.menubar.Enable(ID_COLLECTTO, 0)
        
        self.autoscale = 1
        
        # A status bar to tell people what's happening
        self.CreateStatusBar(1)
        
        lc = wx.ListCtrl(self, -1, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.lc = lc
        self.lc.InsertColumn(0, "Number")
        self.lc.InsertColumn(1, "File name")
        self.lc.SetColumnWidth(0, 100)
        self.lc.SetColumnWidth(1, 400)
        self.Show(True)
            
    def OnChooseFolder(self, event):
        """
        Shows a directory select dialog, allows to set the self.defaultdirname
        to a value where data collection will occur, also enables the menu item
        corresponding to data collection.
        """
        dlg = wx.DirDialog(frame, "Choose a folder with MSFit files:",
        style=wx.DD_DEFAULT_STYLE)
        # If the user selects OK, then we process the dialog's data.
        # This is done by getting the path data from the dialog - BEFORE
        # we destroy it. 
        if dlg.ShowModal() == wx.ID_OK:
            #log.WriteText('You selected: %s\n' % dlg.GetPath())
            self.defaultdirname = dlg.GetPath()
        # Only destroy a dialog after you're done with it.
        dlg.Destroy()
        self.menubar.Enable(ID_COLLECTTO, 1)
        self.filelist = Read_Dir(self.defaultdirname)
        self.lc.DeleteAllItems()
        #self.txtout.WriteText("You chose "+self.defaultdirname+" that contains the following html files:\n\n")
        counter = 0
        for filename in self.filelist:
            filepath = os.path.split(filename)
            self.lc.InsertStringItem(counter, str(counter+1))
            self.lc.SetStringItem(counter, 1, filepath[1])
            counter+=1
        self.SetStatusText("Now choose where to save the output file (File>Collect data to...)")
        
    def ProcessFiles(self, filelist):
        biglist = []
        outlist = []
        progcounter = 0
        self.progbar = wx.ProgressDialog("Quantitating peptides...", "Working...",
                        maximum = len(self.filelist), parent = self,
                        style = wx.PD_CAN_ABORT| wx.PD_APP_MODAL)
        for file in filelist:
            shortname = os.path.split(file)[1]
            try:
                f = open(file, "r")
                htmlpage = f.read()
                f.close
            
                parser = MSFit32ParserProt()
                parser.feed(htmlpage)
                parser.close()

                biglist.append([file, parser.get_dictionary()])
            
                self.progbar.Update(progcounter, str(progcounter)+"    "+shortname+".")
                progcounter+=1
            
            except:
                pass
            
        #print len(biglist)
        self.progbar.Destroy()

        for entrieslist in biglist:
            #print entrieslist
            for dict in entrieslist[1]:
                #print dict
                outlist.append([entrieslist[0], dict['protdata']['rank'],
                dict['protdata']['protein_coverage'], dict['protdata']['protein_mw'],
                dict['protdata']['protein_pI'], dict['protdata']['accession_number'],
                dict['protdata']['entry_name']])
        return outlist
        
    def OnCollectData(self, event):
        """
        Save data to a specified text file (new)
        """
        wildcard = "Tab delimited text file (*.txt)|*.txt|"\
                    "All files (*.*)|*.*"
        #might be os.getcwd()
        dlg = wx.FileDialog(
            self, message="Save collected data as ...", defaultDir = self.defaultdirname,
            defaultFile="", wildcard=wildcard, style=wx.SAVE
            )
        dlg.SetFilterIndex(0)
        if dlg.ShowModal() == wx.ID_OK:
            #self.dirname=dlg.GetDirectory()
            #self.defaultdirname = self.dirname
            path = dlg.GetPath()
            #print "You selected", path            
            try:
                #filelist = Read_Dir(self.defaultdirname)
                outlist = self.ProcessFiles(self.filelist)
                WriteListToFile(path, outlist)
            except IOError:
                dlg = wx.MessageDialog(self, "Error. Maybe the output file is in use...", 'Problem saving data',
                wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
        dlg.Destroy()
    
    def OnHelpAbout(self, event):
        from wx.lib.dialogs import ScrolledMessageDialog
        about = ScrolledMessageDialog(self, __doc__, "About...")
        about.ShowModal()
        
    def OnFileExit(self, event):
        self.Close()
        

app = wx.PySimpleApp()
frame = MainFrame(None, -1, "MSFit to txt v. 0.2")
app.SetTopWindow(frame)
app.MainLoop() 
