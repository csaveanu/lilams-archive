from HTMLParser import HTMLParser
import re

"""
A module providing the necessary tools to parse
and transform to a data dictionary the first 
entry from an MSFit version 3.2 identification file

Compared to the previous regular expression based parsing
this version is more flexible

Regular expression needed to extract sequences
of peptides from MSFit lines are compiled outside
functions to avoid repetition of re compilations
"""
pepseqRE = r"sequence=([A-Z]+)\*?\\"
peplinePATT = re.compile(pepseqRE)
pepseqmodRE = r"\)([A-Z]+)\*?\("
peplinePATTmod = re.compile(pepseqmodRE)

class MSFit32Parser(HTMLParser):
	"""
	A parser for MSFit HTML results to obtain a
	dictionary of data corresponding to the page
	"""
	def reset(self):
		HTMLParser.reset(self)
		self.parsedlist =[]
		#list of text contained between tags
		self.comments = []
		#list of comment texts
		self.aftercomment = 0
		#true if the text to be handled
		#follows a comment
		self.accesionRE = re.compile('> (.+)')
		#allows extraction of ORF name from the first table
		
		#two variables to allow start of scanning for  peptide sequences in file
		self.seeksequence = False
		self.seeksequencemod = False
		
		self.seqlist = []
		#contains the list of sequences of all peptides
		
	def handle_starttag(self, tag, attrs):
		#print tag, attrs
		if tag.lower() == "nobr":
			self.seeksequencemod = True
		elif tag.lower() == "script":
			self.seeksequence = True
		else:
			self.seeksequencemod, self.seeksequence = False, False
	
	def handle_comment(self, tag):
		"""
		Overrides the handle_comment method
		Builds a list of all the found comments
		If a comment is found, the aftercomment
		variable is set to True
		"""
		self.comments.append(tag)
		self.aftercomment = 1
		#if tag == 'delta_mass':
			#self.seeksequence = True
		
		
	def handle_data(self, text):
		"""
		Overrides the handle_data method
		writes to a list the text following
		any comment - modifies self.parsedlist
		"""
		#print "text", text
		if self.aftercomment:
			cleantext = text.strip('\n\r ./')
			self.parsedlist.append([self.comments[-1],cleantext])
			#print cleantext, self.comments[-1]
			#append the last comment and the text that follows
			self.aftercomment = 0
			#don't get any other value until the next comment
			#appears
			#however obtain the sequence of a peptide if we are in an in_mass tags
		elif self.seeksequence:
			#print text, "under seek sequence"
			pepmatch = peplinePATT.search(text)
			if pepmatch:
				self.seqlist.append(pepmatch.groups()[0])
		elif self.seeksequencemod:
			pepmatch = peplinePATTmod.search(text)
			if pepmatch:
				self.seqlist.append(pepmatch.groups()[0])
		return 1
			
		
	def get_dictionary(self):
		"""
		Reorganizes data and treats some special cases
		of data found after comment tags
		Should be called after closing the parser
		"""
		reccounter = 0
		secreccounter = 0
		list_out = []
		list = self.parsedlist
		details = 0
		passedrank1 = 0
		seq_counter = 0 #a counter for the list of sequences, append to matched masses
		
		for i in range(len(list)):
			if list[i][0] == 'rank':
				passedrank1 = 1
				if list[i+1][0] <> 'fraction_matched':
					#we are not at the beginning of the details
					#sometimes proteins have equal ranks
					#we build the data dictionary for the identified proteins
					reccounter+=1
					list_out.append({'protdata':{'rank':list[i][1]}, \
						'pepdata':{'matched':[], 'unmatched':[]}})
					details = 0
					list_out[reccounter-1]['protdata']['comment'] = list[0][1]
					list_out[reccounter-1]['protdata']['database'] = list[1][1]
					list_out[reccounter-1]['protdata']['parent_mass_tolerance'] = list[3][1]
									
				else:
					#we are at the beginning of the details
					details = 1
					secreccounter+=1
			else:
				if details == 0 and passedrank1 == 1:
					#not yet in details but passed the first rank tags
					#take care first of the accesion numbers
					if list[i][0] == 'accession_number':
						matches = self.accesionRE.search(self.parsedlist[i][1])
						if matches:
							entries = matches.groups()
							list[i][1]=entries[0]
					list_out[reccounter-1]['protdata'][list[i][0]] = list[i][1]
				elif details == 1:
					#we are in the details, we may build the peptide masses lists
					if list[i][0] == 'protein_coverage':
						list_out[secreccounter-1]['protdata'][list[i][0]] = list[i][1]
					elif list[i][0] == 'in_mass':
						if list[i+1][0] == 'matched_mass':
							#we are in the matched masses
							list_out[secreccounter-1]['pepdata']['matched'].append([list[i][1], self.seqlist[seq_counter]])
							
							seq_counter+=1
							#print seq_counter
						else:
							list_out[secreccounter-1]['pepdata']['unmatched'].append(list[i][1])
						

		return list_out
		
class MSFit32ParserProt(HTMLParser):
	"""
	A parser for MSFit HTML results to obtain a
	dictionary of data corresponding to the page
	Only protein information is retained
	"""
	def reset(self):
		HTMLParser.reset(self)
		self.parsedlist =[]
		#list of text contained between tags
		self.comments = []
		#list of comment texts
		self.aftercomment = 0
		#true if the text to be handled
		#follows a comment
		self.accesionRE = re.compile('> (.+)')
		#allows extraction of ORF name from the first table
		
		#two variables to allow start of scanning for  peptide sequences in file
		self.seeksequence = False
		self.seeksequencemod = False
		
		self.seqlist = []
		#contains the list of sequences of all peptides
		
	def handle_starttag(self, tag, attrs):
		#print tag, attrs
		pass
	
	def handle_comment(self, tag):
		"""
		Overrides the handle_comment method
		Builds a list of all the found comments
		If a comment is found, the aftercomment
		variable is set to True
		"""
		self.comments.append(tag)
		self.aftercomment = 1
		#if tag == 'delta_mass':
			#self.seeksequence = True
		
		
	def handle_data(self, text):
		"""
		Overrides the handle_data method
		writes to a list the text following
		any comment - modifies self.parsedlist
		"""
		#print "text", text
		if self.aftercomment:
			cleantext = text.strip('\n\r ./')
			self.parsedlist.append([self.comments[-1],cleantext])
			#append the last comment and the text that follows
			self.aftercomment = 0
			#don't get any other value until the next comment
			#appears
			#however obtain the sequence of a peptide if we are in an in_mass tags
		return 1
			
		
	def get_dictionary(self):
		"""
		Reorganizes data and treats some special cases
		of data found after comment tags
		Should be called after closing the parser
		"""
		reccounter = 0
		secreccounter = 0
		list_out = []
		list = self.parsedlist
		details = 0
		passedrank1 = 0
		
		for i in range(len(list)):
			if list[i][0] == 'rank':
				passedrank1 = 1
				if list[i+1][0] <> 'fraction_matched':
					#we are not at the beginning of the details
					#sometimes proteins have equal ranks
					#we build the data dictionary for the identified proteins
					reccounter+=1
					list_out.append({'protdata':{'rank':list[i][1]}})
					details = 0
				else:
					#we are at the beginning of the details
					details = 1
					secreccounter+=1
			else:
				if details == 0 and passedrank1 == 1:
					#not yet in details but passed the first rank tags
					#take care first of the accesion numbers
					if list[i][0] == 'accession_number':
						matches = self.accesionRE.search(self.parsedlist[i][1])
						if matches:
							entries = matches.groups()
							list[i][1]=entries[0]
					list_out[reccounter-1]['protdata'][list[i][0]] = list[i][1]
				elif details == 1:
					#we are in the details, we may build the peptide masses lists
					if list[i][0] == 'protein_coverage':
						list_out[secreccounter-1]['protdata'][list[i][0]] = list[i][1]
		return list_out
		
#an entry in the list_out looks like this:
#{'comment': '120804 CS X07', 'protein_mw': '115945.3', 'protein_coverage': '26%', 'mowse_score': '1.64e+007', 'database': 'PA.yeastorf', 'accession_number': 'ORFP:YLR249W', 'rank': '1', 'entry_name': '>ORFP:YLR249W YEF3, Chr XII from 636780-639914', 'parent_mass_tolerance': '50.000', 'protein_pI': '5.73', 'species': 'UNREADABLE'}
		
	
#~ f = open('/Volumes/Fire/CosminF/200409September/SILAC_Rlp24/Msfit/cs_x07b.htm', 'r')
#~ htmlpage = f.read()
#~ f.close()		
#~ parser = MSFit32Parser()
#~ parser.feed(htmlpage)
#~ parser.close()

#~ result = parser.get_dictionary()
#~ for record in result:
	#~ print record['protdata']
