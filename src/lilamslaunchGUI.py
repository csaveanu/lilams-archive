#!/usr/bin/env pythonw
"""
LilamslaunchGUI.py is a GUI for the lilamsxml.py script
Allows interactive choosing of a file containing
pairs of Spectrum/MSfit file names (complete path)
and of a file containing ORF names of interest

The output consists of xml files with the same name
as the msfit html files used for quantitation.

Cosmin, IP - September 2004
Modified in Jan 2008 to be able to locate the
lilamsxml script and the ipc executable path
Updated the import of the ElementTree XML module
to use the one integrated in Python 2.5
"""

import os, string, sys, re, wx
from string import lstrip, rstrip
#from elementtree import ElementTree as ET
#import cElementTree as ET
import xml.etree.cElementTree as ET
#works with Python 2.5 and higher

def ReadFromFile(filename, skip=0):
    """
    Obtain data from a tab delimited file into a list
    
    Needs the import string statement
    The <skip> parameter indicate the number of lines not
    read.
    
    """
    data = []
    f=open(filename, 'r')
    for i in range(skip):
        f.readline()
    while (1):
        line = f.readline()
        if line == "": break
        data.append(line.strip().split('\t'))
    f.close()
    return data
    #the returned data will be in string format


#generate IDs for the interface
[ID_GO, ID_EXIT, ID_ABOUT, ID_PAIRS, ID_ORFS, ID_IPC, ID_LILAMS] = map(lambda init_ctrls: wx.NewId(), range(7))

class MainFrame(wx.Frame):
    
    def __init__(self, parent, id, title):    
        wx.Frame.__init__(self, parent, id, title,
        wx.DefaultPosition, (600, 200))
            
        #default directory for saving, will be modified after first save
        self.defaultdirname = "."
        
        self.orffilename_ok = False
        self.pairsfilename_ok = False
        self.ipc_path = ""
        self.lilams_path = ""

        # Create the menu bar and items
        self.menubar = wx.MenuBar()
        
        filemenu = wx.Menu()
        filemenu.Append(ID_PAIRS, '&Open file with pairs...', 'Open a file that contains pairs of complete file names separated by tabs - spectrum file .. Msfit identification file')
        filemenu.Append(ID_ORFS, 'Open f&ile with ORFs...', 'Open a list of ORFs to be considered for calculations')
        filemenu.Append(ID_IPC, 'Choose &IPC location...', 'Choose the ipc program path.')
        filemenu.Append(ID_LILAMS, 'Choose Li&lams.py location...', 'Choose the lilamsxml script.')
        filemenu.AppendSeparator()
        filemenu.Append(ID_GO, '&Run...\tCtrl+R', 'Run: perform calculations on the dataset')
        filemenu.AppendSeparator()
        filemenu.Append(ID_EXIT, '&Quit\tCtrl+Q', 'Exit program')
        
        helpmenu = wx.Menu()
        helpmenu.Append(ID_ABOUT, 'About...\tCtrl+E', 'About this program')
        
        # Bind the events to methods 
        wx.EVT_MENU(self, ID_PAIRS, self.OnFileOpenPairs)
        wx.EVT_MENU(self, ID_ORFS, self.OnFileOpenORFs)
        wx.EVT_MENU(self, ID_IPC, self.OnIPCChoose)
        wx.EVT_MENU(self, ID_LILAMS, self.OnLilamsChoose)
        wx.EVT_MENU(self, ID_GO, self.OnGo)
        wx.EVT_MENU(self, ID_EXIT, self.OnFileExit)
        wx.EVT_MENU(self, ID_ABOUT, self.OnHelpAbout)
        
        # Attach menus to the menubar
        self.menubar.Append(filemenu, '&File')
        self.menubar.Append(helpmenu, '&Help')
            
        self.SetMenuBar(self.menubar)
        #disable by default the Run menu
        self.menubar.Enable(ID_GO, 0)
        
        # A status bar to tell people what's happening
        self.CreateStatusBar(1)
        self.Show(True)
    
    def OnFileOpenORFs(self, event): 
        """
        Obtain the name of the file with the ORFs list
        Set the self.orfname_ok to True
        """
        wildcard = "Text file (*.txt)|*.txt|"\
                    "All files (*.*)|*.*"
        dlg = wx.FileDialog(self, "Choose the interesting ORFs list text file",
            self.defaultdirname, "", wildcard, wx.OPEN) 
        if dlg.ShowModal() == wx.ID_OK: 
            filename=dlg.GetFilename() 
            dirname=dlg.GetDirectory()
            self.defaultdirname = dirname
            self.orffilename = os.path.join(dirname, filename)
            dlg.Destroy()
            self.orffilename_ok = True
        else:
            self.orffilename_ok = False
        if self.orffilename_ok == True and self.pairsfilename_ok == True:
            self.menubar.Enable(ID_GO, 1)
        else:
            self.menubar.Enable(ID_GO, 0)
            
    def OnFileOpenPairs(self, event):
        """
        Obtain the name of the file with the pairs
        spectrum files/msfit files - complete paths
        """
        wildcard = "Tab delimited text file (*.txt)|*.txt|"\
                    "All files (*.*)|*.*"
        dlg = wx.FileDialog(self, "Choose the tab delimited text file with spectra/msfit",
            self.defaultdirname, "", wildcard, wx.OPEN) 
        if dlg.ShowModal() == wx.ID_OK: 
            filename=dlg.GetFilename() 
            dirname=dlg.GetDirectory()
            self.defaultdirname = dirname
            self.pairsfilename = os.path.join(dirname, filename)
            dlg.Destroy()
            #self.menubar.Enable(ID_SAVEDATA, 0)
            self.pairsfilename_ok = True
        else:
            self.pairsfilename_ok = False
            
        if self.orffilename_ok == True and self.pairsfilename_ok == True:
            self.menubar.Enable(ID_GO, 1)
        else:
            self.menubar.Enable(ID_GO, 0)

    def OnIPCChoose(self, event): 
        """
        Obtain the path to the IPC executable with a dialog
        Set the self.ipc_path
        """
        wildcard = "All files (*.*)|*.*"
        dlg = wx.FileDialog(self, "Choose the interesting ORFs list text file",
            "/usr/local/", "", wildcard, wx.OPEN) 
        if dlg.ShowModal() == wx.ID_OK: 
            filename=dlg.GetFilename() 
            dirname=dlg.GetDirectory()
            self.ipc_path = os.path.join(dirname, filename)
            dlg.Destroy()
            #self.menubar.Enable(ID_SAVEDATA, 0)
            
    def OnLilamsChoose(self, event): 
        """
        Obtain the path to the python program used for calculations with a dialog
        Set the self.lilams_path
        """
        wildcard = "Python files (*.py)|*.py"
        dlg = wx.FileDialog(self, "Choose the lilams xml python script to run",
            ".", "", wildcard, wx.OPEN) 
        if dlg.ShowModal() == wx.ID_OK: 
            filename=dlg.GetFilename() 
            dirname=dlg.GetDirectory()
            self.lilams_path = os.path.join(dirname, filename)
            dlg.Destroy()
            #self.menubar.Enable(ID_SAVEDATA, 0)
                    
    def OnGo(self, event):
        #scriptname = r"python lilamsxml.py"
        scriptname = "python " + self.lilams_path
        ipccommand = "-c " + self.ipc_path
        
        if "" not in [self.ipc_path, self.lilams_path]:
            #HTMLbasepath = "/Volumes/Fire/CosminF/200409September/SILAC_Rlp24/Msfit/"
            #SPECTRAbasepath = "/Volumes/Fire/CosminF/200409September/SILAC_Rlp24/Spectra/"
            #datalist = ReadFromFile(HTMLbasepath+"Results/pairsspectmsfitOK.txt")
            datalist = ReadFromFile(self.pairsfilename)

            #orffilename = HTMLbasepath+"Results/listeORFpreriboetcontam.tab"
            orffilecomm = "-p " + self.orffilename

            print len(datalist), "pairs of files to be analysed"
            #print datalist
            #print datalist
            paircounter = 0
            dlg = wx.ProgressDialog("Quantitating peptides...", "Working...", maximum = len(datalist), parent = self,
                style = wx.PD_CAN_ABORT| wx.PD_APP_MODAL)

            for pair in datalist:
                #generate as many xml files as html files are present

                #the pair contains the name of the spectrum file and the name of the msfit file
                spectrumname = "-s " + pair[0]
                identifname = "-i " + pair[1]

                tailspecfile = os.path.split(pair[0])[1]
                splittedid = os.path.splitext(tailspecfile)
                splittedpath = os.path.split(pair[1])
                splittedfname = os.path.splitext(splittedpath[1])
                xmlfname = splittedfname[0] + "_" +splittedid[0] + ".xml"
                resultfname = os.path.join(splittedpath[0], xmlfname)

                try:
                    pipe_s = os.popen("%s %s %s %s %s" % (scriptname, spectrumname, identifname,
                                          ipccommand, orffilecomm))
                    result = pipe_s.read()
                    #the result is an xml formatted string
                    if result <> "nodata":
                        fileout = open(resultfname, "w")
                        fileout.write(result)
                        fileout.close()
                    paircounter+=1
                    print paircounter, pair[0], pair[1], "done..." 
                    dlg.Update(paircounter, "Done "+ xmlfname +".")
                except:
                    print pair, "didn't work"
            dlg.Destroy()
        
        else:
            dlg = wx.MessageDialog(self, 'IPC path and lilams script must be specified',
                      'Alert', wx.OK | wx.ICON_INFORMATION)
            try:
                result = dlg.ShowModal()
                if  result == wx.ID_OK:
                    dlg.Destroy()
            finally:
                dlg.Destroy()        
            
    def OnHelpAbout(self, event):
        from wx.lib.dialogs import ScrolledMessageDialog
        about = ScrolledMessageDialog(self, __doc__, "About...")
        about.ShowModal()
        
    def OnFileExit(self, event):
        self.Close()
        


app = wx.PySimpleApp()
frame = MainFrame(None, -1, "LilamsXML launcher v. 0.2")
app.SetTopWindow(frame)
app.MainLoop() 

