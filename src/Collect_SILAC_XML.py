"""
Collect_SILAC_XML.py

Allow collection of SILAC XML file results after
validation, calculates statistics and generates
a table with results, aggregating the results
for each protein (as an option)

Usage: python Collect_SILAC_XML.py -f <folder with XML SILAC files> -s
(switch to obtain the output in aggregated or summary form)

Output is to stdout

First argument - folder with XML files
Output - table in tab delimited format containing
the statistics for each validated protein, printed to stdout

This script is provided "as is" and works well with correctly
formatted XML SILAC files. However, no exceptions have been
implemented and the code will fail if not fed correct data

Cosmin Saveanu, last modifications 2008-04-14
Genetique des Interactions Macromoleculaires, 
Alain Jacquier's lab, Institut Pasteur,
CNRS-URA 2171, France
csaveanu@pasteur.fr for any questions
"""

#import cElementTree as ET (for versions of Python < 2.5)
import xml.etree.cElementTree as ET #latest version integrated in Python 2.5
import sys, os, math, getopt

#a dictionary that holds the position of the different
#informations available in the SILAC XML file
#used by GetValXML
XMLdict = {'pepseq': 0, 'mzh':1, 'calc_mzh':2, 'leucines':3,
            'mono_iso_int':4, 'deuterated_intensity':5,
            'ratio':6, 'validated':7,
            'local_bg':8, 'corrected_ratio':9}
                

def Mean(inlist):
    """
    Obtain the mean of the values from a list
    Returns 0 if the list is empty
    """ 
    sum = 0
    for item in inlist:
        sum = sum + item
    if len(inlist)>0:
        return sum/float(len(inlist))
    else:
        return 0

def SumSquares(inlist):
    """
    Squares each value in the passed list, adds up these squares and
    returns the result.
    """
    ss = 0
    for item in inlist:
        ss = ss + item*item
    return ss


def Variance(inlist):
    """
    Calculates the variance for a sample    
    Returns 100 if less than 2 values are provided
    """
    n = len(inlist)
    mn = Mean(inlist)
    ssq = SumSquares(inlist)
    if n >1:
        vari = (ssq - n*mn*mn)/(n-1)
    else:
        vari = 100
    return vari

def StdDev(inlist):
    """
    Returns the standard deviation of the values in the passed list
    using N-1 in the denominator (i.e., to estimate population stdev).

    """
    return math.sqrt(Variance(inlist))


def Median(inlist):
    """
    Returns the median of the passed list.
    Returns 0 or the element if less than 2 elements
    are passed to the function
    """
    newlist = inlist
    newlist.sort()
    if len(newlist) == 0:
        median = 0
    elif len(newlist) == 1:
        median = newlist[0]
    elif len(newlist) % 2 == 0:   # if even number, simple mean of middles
        index = len(newlist)/2
        median = float(newlist[index] + newlist[index-1]) /2
    else:
        index = len(newlist)/2
        median = newlist[index]
    return median



def get_flist(dirname, ext = ".xml"):
    """
    Get all .ext file names from dirname, not recursive
    returns list of complete paths for files
    """
    outlist = []
    for f in os.listdir(dirname):
        file = dirname + os.sep + f
        if os.path.isdir(file):
            pass
            #we do nothing with the subfolders
        else:
            extension = os.path.splitext(f)[1]
            if extension in (ext,):
                try:
                    outlist.append(file)
                except:
                    pass
    return outlist
    
def GetValXML(data, index=0, what='pepseq'):
    """
    Returns the text attribute of an entry in an ElementTree
    object based on the XMLdict dictionary that maps
    numbers to tags - definition in the init of the main
    frame. By default it returns the peptide sequence of 
    the first record.
    
    pdatapos - indicates the position of the <pepdata> in
    the root of the pepdata xml entry
    """
    pdatapos = 5
    return data[pdatapos][index][XMLdict[what]].text
    
def GenerateListDict(xmldata):
    """
    Recovers the data about a peptide in the XML SILAC file
    Returns a dictionary, each peptide has an integer as key
    """
    entriesdict = {}
    for i in range(len(xmldata[5])):
        entriesdict[i] = (GetValXML(xmldata, i, 'pepseq'),
                GetValXML(xmldata, i, 'leucines'),
                GetValXML(xmldata, i, 'mzh'),
                GetValXML(xmldata, i, 'calc_mzh'),
                GetValXML(xmldata, i, 'mono_iso_int'),
                GetValXML(xmldata, i, 'deuterated_intensity'),
                GetValXML(xmldata, i, 'ratio'),
                GetValXML(xmldata, i, 'corrected_ratio'),
                GetValXML(xmldata, i, 'local_bg'),
                GetValXML(xmldata, i, 'validated'))
    return entriesdict

def CalcStatistics(datadict):
    """
    Returns statistics on a single data dictionary (corresponding to one protein)
    as a multiple of strings <nr of valid peptides>, <median>, <mean>, <SD>
    """
    yeslist = []
    yeskeylist = []
    for key in datadict.keys():
        if datadict[key][9] == "YES":
            yeskeylist.append(key)
            yeslist.append(float(datadict[key][7]))
            #7 is calculated corrected ratio index
    median = Median(yeslist)
    mean = Mean(yeslist)
    stddev = StdDev(yeslist)
    nopep = len(yeslist)
    
    return("%d"%nopep, "%.2f"%median, "%.2f"%mean, "%.4f"%stddev)
    
   
def ExtractSILACDataFromXML(fname, peptides_also=True):
    """Parses an XML Silac file and returns information
    on all the validated peptides in the entry if peptides_also
    is True. The origin of the peptide (protein) is
    also recovered and used in the output. Uses:
    - GenerateListDict - to obtain the peptide data
    - CalcStatistics - to calculate protein SILAC statistics. 
    Returns a string, formatted differently if peptides_also
    is true or false
    """
    tree = ET.parse(fname)
    data = tree.getroot()
    fsplit = os.path.split(fname)
    endofname = fsplit[1]
    #the root is a <protdata> entry
    prot_data = "\t".join([endofname,
                   data[0][3].text,
                   data[0][1].text,
                   data[0][2].text,
                   data[0][0].text,
                   data[2].text,
                   data[1].text,
                   data[0][4].text[:-1],
                   data[0][5].text])
    #print "> Mowse score: %s, database: %s, ppm tolerance: %s"%(data[0][3].text,
    #        data[0][1].text, data[0][2].text)
    #print "> Spot identification: %s.\n"%data[0][0].text
    #print "> Identified protein: %s, accesion number: %s."%(data[2].text, data[1].text)
    #print "> Peptide coverage: %s percent, matched peptides: %s."%(data[0][4].text[:-1], data[0][5].text)
    datadict = GenerateListDict(data)
    stats = CalcStatistics(datadict)
    stats_string = "\t".join(stats)
    if peptides_also:
        peptide_info_list = []
        for peptkey in datadict.keys():
            if datadict[peptkey][9] == "YES":
                peptide_info_list.append("\t".join([prot_data, "\t".join(datadict[peptkey]), stats_string]))
        return "\n".join(peptide_info_list)
    else:
        return "\t".join([prot_data, stats_string])


class Usage(Exception):
    """
    used for catching exceptions generated
    during command line parameter parsing
    """
    def __init__(self, msg):
        self.msg = msg
        
def main(argv=None):
    global foldername, aggregate
    
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "hsf:", ["help", "summary",
                "XMLfolder="])
        except getopt.error, msg:
            raise Usage(msg)
        # option processing
        
        foldername = ""
        aggregate = False
        for option, value in opts:
            if option in ("-h", "--help"):
                raise Usage(__doc__)
            if option in ("-s", "--summary"):
                aggregate = True
            if option in ("-f", "--XMLfolder"):
                foldername = value
                
        if "" in (foldername,):
            raise Usage("no input folder!")
            sys.exit(2)
        else:
            try:
                flist = get_flist(foldername)

                header_identif = "\t".join(["file_name",
                                            "Mowse_score",
                                            "Database",
                                            "ppm_tolerance",
                                            "Spot_identification",
                                            "Identified_protein",
                                            "Accesion_number",
                                            "Peptide_coverage",
                                            "%_matched_peptides"])
                header_stats = "\t".join(["Number_of_valid_peptides",
                                          "Median_ratio", "Mean_ratio",
                                          "Std_Dev"])
                if not aggregate:
                    header_pep = "\t".join(['pepseq',
                                        'leucines',
                                        'mzh',
                                        'calc_mzh',
                                        'mono_iso_int',
                                        'deuterated_intensity',
                                        'ratio',
                                        'corrected_ratio',
                                        'local_bg',
                                        'validated'])
                    header = "\t".join([header_identif, header_pep, header_stats])
                else:
                    header = "\t".join([header_identif, header_stats])
                print header
                for fname in flist:
                    print ExtractSILACDataFromXML(fname, peptides_also= not aggregate)
                
            finally:
                pass
               
    except Usage, err:
        print >>sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
        print >>sys.stderr, "\t for help use --help or -h"
        return 2    
    
if __name__ == "__main__":
    sys.exit(main())
