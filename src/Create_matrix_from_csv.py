"""
Will take a list of files as input, read data, and output
a matrix of values. Used to see the SILAC data.
"""

import sys, getopt, pdb, os

def Read_csv_rem_header(infname, sep='\t'):
    """Reading csv file, separator should be provided, default is tab
    """
    fin = open(infname, 'rU')
    throw = fin.readline()
    data = [line.strip().split(sep) for line in fin]
    fin.close()
    return data

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "hi:o:", ["help", "output=", "input"])
        except getopt.error, msg:
             raise Usage(msg)

        # option processing
        for option, value in opts:
            if option in ("-i", "--input"):
                inputlist = value
                flist = open(inputlist, 'r')
                fvalues = [f.strip() for f in flist]
                flist.close()
            if option in ("-h", "--help"):
                raise Usage(__doc__)
            if option in ("-o", "--output"):
                output = value

        datalist = {}
        for filename in fvalues:
            p = os.path.split(filename)
            datalist[p[1]]=Read_csv_rem_header(filename, sep=',')

        proteindict = {}
        
        for f in datalist.keys():
#            pdb.set_trace()
            for protline in datalist[f]:
                if proteindict.has_key(protline[1]):
                    proteindict[protline[1]].append((f, protline[2]))
                else:
                    proteindict[protline[1]] = [(f, protline[2])]
#        print proteindict
        header = datalist.keys()
        result = [header]
        for protein in proteindict.keys():
            resprot= [protein]
            for file in header:
                files_available = [f_v[0] for f_v in proteindict[protein]]
                if file in files_available:
                    for file_val in proteindict[protein]:
                        if file == file_val[0]:
                            resprot.append(file_val[1])
                else:
                    resprot.append("NA")
            result.append(resprot)
        for res in result:
            print '\t'.join(res)
            

    except Usage, err:
        print >>sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
        print >>sys.stderr, "\t for help use --help"
        return 2

if __name__ == "__main__":
    sys.exit(main())
